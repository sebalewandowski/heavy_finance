<?php

namespace App\Supplier;

class Xml extends SupplierAbstract
{
    public static function getName(): string
    {
        return 'xml';
    }

    protected function parseResponse(): array
    {
        $xml = simplexml_load_string($this->getResponse());
        $json = json_encode($xml);

        return json_decode($json,TRUE);
    }

    protected function getResponse(): string|bool
    {
        return file_get_contents($_ENV['XML_SUPPLIER_URL']);
    }
}