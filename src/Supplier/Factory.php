<?php

namespace App\Supplier;

use InvalidArgumentException;

class Factory implements FactoryInterface
{
    /**
     * @param string $supplierName
     * @return SupplierInterface
     */
    public function getSupplier(string $supplierName): SupplierInterface
    {
        $supplierClass = __NAMESPACE__ . '\\' . ucfirst($supplierName);

        if (!class_exists($supplierClass)) {
            throw new InvalidArgumentException(
                sprintf('The class "%s" does not exist.', $supplierName)
            );
        }

        $supplier = new $supplierClass();

        if ($supplier instanceof SupplierAbstract) {
            return $supplier;
        }
    }
}
