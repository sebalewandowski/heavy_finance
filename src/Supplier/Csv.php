<?php

namespace App\Supplier;

class Csv extends SupplierAbstract
{
    public static function getName(): string
    {
        return 'csv';
    }

    protected function parseResponse(): array
    {
        $csv = array_map("str_getcsv", $this->getResponse());
        $keys = array_shift($csv);
        $data = [];

        foreach ($csv as $i => $row) {
            $data[$i] = array_combine($keys, $row);
        }

        return $data;
    }

    protected function getResponse()
    {
        return file($_ENV['CSV_SUPPLIER_URL'],FILE_SKIP_EMPTY_LINES);
    }
}