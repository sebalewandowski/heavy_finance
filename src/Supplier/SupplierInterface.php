<?php

namespace App\Supplier;


interface SupplierInterface
{
    public function getData(): array;
    public static function getName(): string;
}
