<?php

namespace App\Supplier;

use App\Exception\InvalidParserException;

abstract class SupplierAbstract implements SupplierInterface
{
    /**
     * @return array
     * @throws InvalidParserException
     * @throws \Exception
     */
    abstract protected function parseResponse(): array;

    /**
     * @return array
     * @throws \Exception
     * @throws InvalidParserException
     */
    public function getData(): array
    {
        return $this->parseResponse();
    }
}
