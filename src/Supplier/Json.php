<?php

namespace App\Supplier;

class Json extends SupplierAbstract
{
    public static function getName(): string
    {
        return 'json';
    }

    public static function getResponseType(): string
    {
        return 'json';
    }

    protected function parseResponse(): array
    {
        return json_decode($this->getResponse());
    }

    protected function getResponse(): string|bool
    {
        return file_get_contents($_ENV['JSON_SUPPLIER_URL']);
    }
}