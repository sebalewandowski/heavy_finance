<?php

include_once 'vendor/autoload.php';

use App\Supplier\Factory;

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

if (!isset($argv[1]) || !is_file($infile = $argv[1])) {
    $factory = new Factory();

    // available supplierNames(xml, json, csv)
    $supplier = $factory->getSupplier('xml');
    print_r($supplier->getData());
    exit(1);
}